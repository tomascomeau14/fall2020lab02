//Thomas Comeau 1934037
import bikes.Bicycle;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] bikes = new Bicycle[4];
		bikes[0] = new Bicycle("Ham", 7, 55.4);
		bikes[1] = new Bicycle("BikeLovers", 10, 67.34);
		bikes[2] = new Bicycle("The better gears", 34, 76.4);
		bikes[3] = new Bicycle("Mountain bikes", 989, 100000.1);
		for(int i = 0; i< bikes.length; i++) {
			System.out.println(bikes[i]);
		}
	}
}
