// Thomas Comeau 1934037
package bikes;
public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manu, int numbGears, double maxSpeed) {
		manufacturer = manu;
		numberGears = numbGears;
		this.maxSpeed = maxSpeed;
	}
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	public int getNumberGears() {
		return numberGears;
	}
	
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	public String toString() {
		return ("Manufacturer: "+manufacturer+" ,Number of Gears: "+numberGears+" ,Max speed: "+maxSpeed);
	}
}
